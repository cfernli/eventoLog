package com.itau.jogovelha.controller;

import com.itau.jogovelha.model.Jogada;
import com.itau.jogovelha.model.Placar;
import com.itau.jogovelha.model.Rodada;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.itau.jogovelha.model.Tabuleiro;
import com.itau.jogovelha.service.Message;


@Controller
@CrossOrigin
public class JogoController {

	Rodada rodada = new Rodada("Jogador 1", "Jogador 2");

	@RequestMapping("/")
	public @ResponseBody
	Placar getPlacar() throws Exception {
		Message.sendMessage("Criou placar");
		return rodada.getPlacar();
	}

    @RequestMapping(path = "/", method = RequestMethod.POST)
    public @ResponseBody
    Placar jogar(@RequestBody Jogada jogada) throws Exception {
        rodada.jogar(jogada.x, jogada.y);
        Message.sendMessage("Efetuou jogada");
        return rodada.getPlacar();
    }

    @RequestMapping("/iniciar")
    public @ResponseBody
    Placar iniciarJogo() throws Exception {
        rodada.iniciarJogo();
        Message.sendMessage("Iniciou jogo");
        return rodada.getPlacar();
    }
}
