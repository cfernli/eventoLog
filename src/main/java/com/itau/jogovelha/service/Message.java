package com.itau.jogovelha.service;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Message {
//	private final static String QUEUE_NAME = "mtech";
	private final static String QUEUE_NAME = System.getenv("AMQP_QUEUE");
	public static void sendMessage(String message) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
//		factory.setHost("localhost");
		factory.setHost(System.getenv("AMQP_ADDRESS"));
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();

		channel.queueDeclare(QUEUE_NAME, true, false, false, null);
		channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
		System.out.println(" [x] Sent '" + message + "'");

		channel.close();
		connection.close();
	}

}
